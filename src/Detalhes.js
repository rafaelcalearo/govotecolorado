import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import * as firebase from 'firebase/app'
import 'firebase/firestore'

import Notas from './Notas'

export default class Detalhes extends Component {

  state = {
    id: '', camisa: 0, completo: '', vulgo: '', nome: '', estatura: '', nascimento: '', posicao: '', jogos: 0, gols: 0,
    foto: '', maximo: 0, minimo: 0, nome_votante: '', email_votante: '', fone_votante: '', nota_votante: 0,
    aviso: '', tipo_aviso: '', botao: '', notas: [], height: 0, media: 0
  }

  // obtém os parâmetros passados para a classe e calcula as notas anteriores dadas ao jogador
  componentDidMount() {
    const { match: { params } } = this.props
    const db = firebase.firestore()
    var docRef = db.collection("jogadores").doc(params.id);

    docRef.get().then(doc => {
      if (doc.exists) {
        this.setState({ id: doc.id, ...doc.data() })
      } else {
        console.log("Erro...");
      }
    }).catch(function (error) {
      console.log("Erro de conexão: ", error);
    });

    let notas = [];

    firebase.firestore().collection('jogadores').doc(params.id)
      .collection("notas" + params.id).onSnapshot(snapshot => {
        snapshot.docChanges().forEach((change) => {
          notas.push({
            id: change.doc.id,
            ...change.doc.data()
          })
        })
        var media = 0;
        for (let index = 0; index < notas.length; index++) {
          media += notas[index].nota;
        }
        if (media > 0) {
          this.setState({ media: (media / notas.length) });
        }
      })
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()

    if (this.state.nome_votante === '') {
      this.setState({ tipo_aviso: 'ERRO' })
      this.setState({ aviso: 'Atenção! O campo de nome não pode ser vazio.' })
      this.tempoAviso()
      return
    } else {
      var nomeValido = this.state.nome_votante;
    }
    if (this.state.email_votante === '') {
      this.setState({ tipo_aviso: 'ERRO' })
      this.setState({ aviso: 'Atenção! O campo de e-mail não pode ser vazio.' })
      this.tempoAviso()
      return
    } else {
      var emailValido = this.state.email_votante;
    }
    if (this.state.fone_votante === '') {
      this.setState({ tipo_aviso: 'ERRO' })
      this.setState({ aviso: 'Atenção! O campo de telefone não pode ser vazio.' })
      this.tempoAviso()
      return
    } else {
      var foneValido = this.state.fone_votante;
    }
    if (this.state.nota_votante < this.state.minimo || this.state.nota_votante > 10) {
      this.setState({ tipo_aviso: 'ERRO' })
      this.setState({ aviso: 'Atenção! O campo de nota não pode ser fora do intervalo de notas entre 1–10' })
      this.tempoAviso()
      return
    } else {
      var notaValida = this.state.nota_votante;
    }

    const nota = {
      nome: nomeValido, email: emailValido, fone: foneValido, nota: parseInt(notaValida), ultimo: 0
    }

    const db = firebase.firestore()

    try {
      db.collection('jogadores').doc(this.state.id)
        .collection('notas' + this.state.id).add(nota)
      this.setState({ tipo_aviso: 'OK' })
      this.setState({ aviso: 'Atenção! Nota cadastrada com sucesso.' })
      this.setState({ botao: 'OK' })
    } catch (erro) {
      this.setState({ tipo_aviso: 'ERRO' })
      this.setState({ aviso: 'Erro: ' + erro })
      this.setState({ botao: '' })
    }
    this.tempoAviso()
  }

  tempoAviso = () => {
    setTimeout(() => { this.setState({ aviso: '' }) }, 3000)
  }

  showNotas = () => {
    let notas = [];

    firebase.firestore().collection('jogadores').doc("" + this.state.id)
      .collection("notas" + this.state.id).onSnapshot(snapshot => {
        snapshot.docChanges().forEach((change) => {
          notas.push({
            id: change.doc.id,
            ...change.doc.data()
          })
        })
        notas.sort(ordenarNotas);
        function ordenarNotas(a, b) {
          return a.nota - b.nota;
        }
        for (let index = 0; index < notas.length; index++) {
          if (this.state.nome_votante === notas[index].nome) {
            notas[index].ultimo = 1;
          }
        }
        notas.reverse();
        this.setState({ notas })
      })
  }

  render() {
    let aviso;

    if (this.state.tipo_aviso === "OK") {
      aviso = <div className='alert alert-info mt-3'>
        {this.state.aviso}
      </div>
    } else {
      aviso = <div className='alert alert-danger mt-3'>
        {this.state.aviso}
      </div>
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3 className="mt-2 mb-0">VOTE</h3>
            <h6 className="mt-0 mb-3 text-muted">VOTE DANDO SUA NOTA NO JOGADOR ESCOLHIDO</h6>
            <div className="card-group">
              <div className="card">
                <img src={this.state.foto} className="card-img-top img-fluid" alt="Foto" />
                <div className="card-footer text-center">
                  <small className="text-muted"><strong>CAMISA:</strong> {this.state.camisa}</small>
                </div>
              </div>
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title mb-0">{this.state.vulgo}</h4>
                  <h6 className="card-subtitle mt-0 mb-4 text-muted">{this.state.completo}</h6>
                  <p className="card-text">
                    <strong>Estatura:</strong> <span>{this.state.estatura} m</span><br />
                    <strong>Nascimento:</strong> <span>{this.state.nascimento}</span><br />
                    <strong>Jogos:</strong> <span>{this.state.jogos}</span><br />
                    <strong>Gols:</strong> <span>{this.state.gols}</span><br />
                    <strong>Posição:</strong> <span><em>{this.state.posicao}</em></span></p>
                </div>
                <div class="card-footer">
                  <small className="text-muted"><strong>MÉDIA DOS VOTOS:</strong> {this.state.media.toFixed(2)}</small>
                </div>
              </div>
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Sua nota aqui</h4>
                  <p className="card-text text-danger">
                    <small>TODOS OS CAMPOS SÃO OBRIGATÓRIOS.
                    NOTA ENTRE: <strong>{this.state.minimo}–{this.state.maximo}</strong>.</small>
                  </p>
                  <form onSubmit={this.handleSubmit}>
                    <div className="input-group mt-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="far fa-user"></i>
                        </span>
                      </div>
                      <input type="text" className="form-control"
                        placeholder="Nome Completo"
                        name="nome_votante"
                        onChange={this.handleChange}
                        value={this.state.nome_votante}
                      />
                    </div>
                    <div className="input-group mt-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fas fa-at"></i>
                        </span>
                      </div>
                      <input type="email" className="form-control"
                        placeholder="E-mail"
                        name="email_votante"
                        onChange={this.handleChange}
                        value={this.state.email_votante}
                      />
                    </div>
                    <div className="input-group mt-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fas fa-phone"></i>
                        </span>
                      </div>
                      <input type="text" className="form-control"
                        placeholder="(99) 99999-9999"
                        name="fone_votante"
                        onChange={this.handleChange}
                        value={this.state.fone_votante}
                      />
                    </div>
                    <div className="input-group mt-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text"><i className="fas fa-vote-yea"></i></span>
                      </div>
                      <input type="number"
                        className="form-control"
                        name="nota_votante"
                        onChange={this.handleChange}
                        value={this.state.nota_votante}
                      />
                    </div>
                    <input type="submit" className="btn btn-sm btn-danger mt-3"
                      value="VOTAR" />
                    <Link to={'/'} className="btn btn-sm btn-secondary ml-1 mt-3">
                      RETORNAR
                  </Link>
                  </form>
                  {this.state.botao !== '' ?
                    <button className="btn btn-sm btn-outline-danger btn-block mt-3" onClick={() => { this.showNotas() }}>
                      EXIBIR NOTAS</button>
                    : ""}
                </div>
                <div class="card-footer">
                  <small style={{ color: "#f7f7f7" }}>|</small>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.aviso !== '' ? aviso : ''}
        <div className="row">
          <div className="col-md-12">
            {this.state.notas.length > 0 ? <h4 className="mt-2">Notas ({this.state.notas.length})</h4> : ''}
            {this.state.notas.map((nota) => (
              <Notas key={nota.id}
                id={nota.id}
                email={nota.email}
                fone={nota.fone}
                nome={nota.nome}
                ultimo={nota.ultimo}
                nota={nota.nota} />
            ))}
          </div>
        </div>
      </div>
    )
  }
}
